﻿using FluentValidation.WebApi;
using RentalAPI.App_Start;
using System.Web.Http;

namespace RentalAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            //Fluent Validation
            config.Filters.Add(new ValidateModelStateFilter());
            
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "RentalAPI",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            FluentValidationModelValidatorProvider.Configure(config);

        }
    }
}
