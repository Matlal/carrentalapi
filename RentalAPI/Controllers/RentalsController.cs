﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using RentalAPI.Models;

namespace RentalAPI.Controllers
{
    [RoutePrefix("api/rentals")]
    public class RentalsController : ApiController
    {
         IRentalAPIContext db = new RentalAPIContext();
        public RentalsController() { }

        public RentalsController(IRentalAPIContext context)
        {
            db = context;
        }

        #region GET
        // GET: api/Rentals
        [Route("")]
        [HttpGet]
        public IQueryable<Rental> GetAllRental()
        {
            return db.Rentals;
        }

        //public async Task<IEnumerable<Rental>> GetAllRentalAsync()
        //{
        //    return await Task.FromResult(GetAllRental());
        //}


        // GET: api/Rentals/5
        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(Rental))]
        public IHttpActionResult GetRental(int id)
        {
            Rental rental = db.Rentals.FirstOrDefault(i=>i.RentalId==id);
            if (rental == null)
            {
                return NotFound();
            }

            return Ok(rental);
        }

        //[ResponseType(typeof(Rental))]
        //public async Task<IHttpActionResult> GetRentalAsync(int id)
        //{
        //    Rental rental = await db.Rentals.FindAsync(id);
        //    if (rental == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(rental);
        //}

        #endregion
        #region POST
        // POST: api/Rentals
        [Route("")]
        [HttpPost]
        public IHttpActionResult Post(Rental rental)
        {
            //product object is validated on ValidateModelStateFilter class  
            if (!ModelState.IsValid) return BadRequest(); //for testing purposes, not needed in production

            if (db.CarRentalCategories.FirstOrDefault(z => z.CategoryId == rental.CategoryId) == null) return BadRequest("CategoryId does not match any stored categories");

            try
            {
                //future code could choose to trust that incoming object from client is valid...
                var obj = new Rental()
                {
                    createdAt = DateTime.Now,
                    updatedAt = DateTime.Now,
                    CategoryId = rental.CategoryId,
                    
                    bookingNumber = getUniqueBookingNumber(db.Rentals),
                    carMilageBefore = rental.carMilageBefore,
                    carRentalCategory = rental.carRentalCategory,
                    customerDateOfBirth = rental.customerDateOfBirth,
                    timeOfRental = rental.timeOfRental

                };
                db.Rentals.Add(obj);
                db.SaveChanges();
                return CreatedAtRoute("RentalAPI", new { id = rental.RentalId }, rental);
            }
            catch
            {
                return InternalServerError();
            }

        }
        public async Task<IHttpActionResult> PostRentalAsync(Rental rental)
        {
            return await Task.FromResult(Post(rental));
        }
        #endregion
        #region PUT

        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRental(int bookingNumber, Rental rental)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);//for testing purposes
            if (bookingNumber != rental.bookingNumber) return BadRequest("non mathing ids");
            
            //db.Entry(product).State = EntityState.Modified;
            var result = Update(rental);
            return result;
        }
        //public async Task<IHttpActionResult> PutRentalAsync(int id, Rental rental)
        //{
        //    return await Task.FromResult(PutRental(id, rental));
        //}
        [HttpPut]
        [Route("{id:int}/Return")]
        public IHttpActionResult PutRentalReturn(int id, Rental rental)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState); //for testing purposes

          
            var result=Update(rental);

            return result;
        }
        private IHttpActionResult Update(Rental _rental)
        {
            //var rentalObj = await db.Rentals.FirstOrDefaultAsync(x => x.RentalId == rental.RentalId);
           var rentalObj = db.Rentals.FirstOrDefault(x => x.bookingNumber == _rental.bookingNumber);
            if (rentalObj==null||rentalObj.bookingNumber != _rental.bookingNumber) return NotFound();
            if (rentalObj.finished) return BadRequest("The rental has already been returned!"); // to enable rental edits efter returnal/finished: could edit this for admin usage (by checking the session cookie). TODO: test this

 
            if (_rental.finished)
            {
                var _milage = (decimal)_rental.carMilageAfter;
                rentalObj = returnRental(rentalObj, _milage, (DateTime)_rental.timeOfReturn);
            }
            rentalObj.updatedAt = DateTime.Now;
            db.MarkAsModified(_rental);
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RentalExists(_rental.RentalId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        #endregion
        #region DELETE
        [HttpDelete]
        [Route("{id:int}")]
        [ResponseType(typeof(Rental))]
        public IHttpActionResult DeleteRental(int id)
        {
            Rental rental = db.Rentals.Find(id);
            if (rental == null)
            {
                return NotFound();
            }

            db.Rentals.Remove(rental);
            db.SaveChanges();

            return Ok(rental);
        }
        public async Task<IHttpActionResult> DeleteRentalAsync(int id)
        {
            Rental rental = await db.Rentals.FindAsync(id);
            if (rental == null)
            {
                return NotFound();
            }

            db.Rentals.Remove(rental);
            db.SaveChanges();

            return Ok(rental);
        }
        #endregion


        #region methods
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RentalExists(int id)
        {
            return db.Rentals.Count(e => e.RentalId == id) > 0;
        }

        //TODO: make these two helper method instead of exposing it as public
        public int getUniqueId(DbSet<Rental> rentals)
        {
            var order = rentals.OrderByDescending(x => x.RentalId);
            return order.Count() == 0 ? 1: order.FirstOrDefault().RentalId+1;       //TODO: test this result
        }
        public int getUniqueBookingNumber(DbSet<Rental> rentals)
        {
            var count = rentals.Count();
            
            if (count == 0 ) return 1;                                              //TODO: test this result
            var nr = rentals.OrderByDescending(x => x.bookingNumber).FirstOrDefault().bookingNumber;

            try{
                nr = nr > 0 ? nr+1:-1;                                              
            }
            catch(Exception e)
            {
                return -1;
            }
            return nr;
        }
       
        private Rental returnRental(Rental rental, Decimal _carMilageAfter, DateTime _timeOfReturn)
        {
            rental.carMilageAfter = _carMilageAfter;
            rental.timeOfReturn = _timeOfReturn;
            rental.finished = true;
            rental = caclulatePrice(rental);
            return rental;
        }
      
        private Rental caclulatePrice(Rental rental)
        {
            var time = rental.timeOfReturn ?? DateTime.Now;
            var numberofdays = new Decimal((time.Date - rental.timeOfRental.Date).TotalDays);
            var milage = rental.carMilageAfter - rental.carMilageBefore;
            decimal timeVariable = rental.carRentalCategory.baseDayRentalVariable.HasValue && rental.carRentalCategory.baseDayRentalVariable > 0 ? (decimal)rental.carRentalCategory.baseDayRentalVariable : 1;
            decimal distVariable = rental.carRentalCategory.distanceVariable.HasValue && rental.carRentalCategory.baseDayRentalVariable > 0 ? (decimal)rental.carRentalCategory.baseDayRentalVariable : 1;
            rental.price =
                rental.carRentalCategory.rentalRate.baseDayRental * (numberofdays) * timeVariable
                +
                rental.carRentalCategory.rentalRate.kmPrice * milage * distVariable;
            return rental;
        }
        #endregion
    }

}