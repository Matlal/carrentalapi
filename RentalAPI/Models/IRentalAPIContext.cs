﻿using System;
using System.Data.Entity;

namespace RentalAPI.Models
{
    public interface IRentalAPIContext : IDisposable
    {
        DbSet<Rental> Rentals { get; }
        DbSet<CarRentalCategory> CarRentalCategories { get; }
        DbSet<RentalRate> RentalRates { get; }
        int SaveChanges();
        void MarkAsModified(Rental item);
        
    }
}