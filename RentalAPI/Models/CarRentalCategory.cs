﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RentalAPI.Models
{
    public class CarRentalCategory
    {
        private const int maxStringlength = 450;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CategoryId { get; set; }                         //Not null


        [Column(TypeName = "VARCHAR")]
        [StringLength(maxStringlength)]
        [Index(IsUnique = true)]
        [Required(ErrorMessage = "The name of the car category is required as a string value")]
        public string name { get; set; }                    //Not null
        
        private DateTime? dateCreated = null;
        public DateTime createdAt
        { get {return dateCreated.HasValue? dateCreated.Value:DateTime.Now;}
          set { dateCreated = value; }
        }

        public DateTime? updatedAt { get; set; } = DateTime.Now;

        public int RentalRateId { get; set; }

        [Required(ErrorMessage = "The id for the CarRentalCategory is required")]
        [ForeignKey("RentalRateId")]
        public RentalRate rentalRate { get; set; }

        public Decimal? baseDayRentalVariable { get; set; } = null;
        public Decimal? distanceVariable { get; set; } = null;

        public ICollection<Rental> Rentals { get; set; }
    }

public class RentalRate
    {

        /// <summary>
        /// A table for registration of new rental prices. 
        /// The last one is the current.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RentalRateId { get; set; }


        //auto set
        private DateTime? dateCreated = null;
        public DateTime createdAt
        {
            get { return dateCreated.HasValue ? dateCreated.Value : DateTime.Now; }
            set { dateCreated = value; }
        }

        public DateTime? updatedAt { get; set; } = DateTime.Now;
        //Not null
        [Required(ErrorMessage = "The base day rental price is required as a Decimal value")]
        public Decimal baseDayRental { get; set; }
        
        //Not null
        [Required(ErrorMessage = "The km price is required as a Decimal value")]
        public Decimal kmPrice { get; set; }                //Not null

        public ICollection<CarRentalCategory> carRentalCategories { get; set; }


    }
}