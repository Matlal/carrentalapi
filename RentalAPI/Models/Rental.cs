﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RentalAPI.Models
{
    [Validator(typeof(RentalValidator))]
    public class Rental
    {
        

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RentalId { get; set; }
        //auto set


        [Required(ErrorMessage = "createdAt is required")]
        public DateTime? createdAt { get; set; }

        public DateTime? updatedAt { get; set; } = DateTime.Now;

        #region incoming data

        public int CategoryId { get; set; }

        [Required(ErrorMessage = "carRentalCategory is required")]
        [ForeignKey("CategoryId")]
        public CarRentalCategory carRentalCategory { get; set; }

        [Index(IsUnique = true)]
        [Required(ErrorMessage = "Booking Number is required as a positive integer")]
        public int bookingNumber { get; set; }

        [Required(ErrorMessage = "Car Milage Before registration is required as a Decimal")]
        public Decimal carMilageBefore { get; set; }                           //Current car milage TODO: decouple database with Car Model (table) , TODO: Create Validation on incoming car milage.  

        [Required(ErrorMessage = "The time of the rental is required as a DateTime value")]
        public DateTime timeOfRental { get; set; }

        [Required(ErrorMessage = "The time of the customers date of birth is required as a DateTime value")]
        public DateTime customerDateOfBirth { get; set; }
        #endregion

        #region resulting data
        public Decimal? carMilageAfter { get; set; } = null;
        public DateTime? timeOfReturn { get; set; } = null;
        public bool finished { get; set; } = false;
        public Decimal? price { get; set; } = null;
        #endregion

        //TODO: (in the future) add viruals so it vill be easy to give options on existing rates and categories, when registering rental  
        //public virtual List<CarRentalCategory> categories { get; set; }
        //public virtual List<RentalRate> rentalRates { get; set; }         
        //public Rental()
        //{
        //    categories = new List<CarRentalCategory>();
        //    rentalRates = new List<RentalRate>();
        //}

        //public bool areInSync(Rental _rental)
        //{
        //    if (bookingNumber != _rental.bookingNumber || customerDateOfBirth != _rental.customerDateOfBirth
        //        || createdAt != _rental.createdAt || updatedAt != _rental.updatedAt || CategoryId != _rental.CategoryId 
        //        || timeOfRental != _rental.timeOfRental || carMilageBefore != _rental.carMilageBefore) return false;

        //    carRentalCategory = _rental.carRentalCategory != carRentalCategory ? _rental.carRentalCategory : carRentalCategory;
        //    return true;
        //}



    }


    public class RentalValidator : AbstractValidator<Rental>
    {
        public RentalValidator()
        {
            ///{PropertyName} = The name of the property being validated
            ///{PropertyValue} = The current value of the property

            //TODO: after adding virtual list (categories), add validation rule to check that the categoryId is valid and matches one of the existing categories.
            #region validate incoming rigistration
            RuleFor(x => x.CategoryId).NotNull().WithMessage("{PropertyName} cannot be null.").GreaterThan(0).WithMessage("{PropertyName} cannot be less than or eual to zero.");
            RuleFor(x => x.timeOfRental).NotNull().GreaterThan(DateTime.Today).WithMessage("{PropertyName} cannot be earlier than registration date.");
            RuleFor(x => x.customerDateOfBirth).NotNull().LessThan(DateTime.Now).WithMessage("{PropertyName} cannot be in the future."); //TODO: add check for specific age (eg 18 or over ?)
            RuleFor(x => x.bookingNumber).NotEmpty();

            #endregion
            #region validate returnal
            RuleFor(x => x.carMilageAfter).GreaterThan(x => x.carMilageBefore).WithMessage("{PropertyName} cannot be less than earlier registration value ('carMilageBefore').").When(m => m.carMilageAfter.HasValue || m.finished ==true);
            RuleFor(x => x.timeOfReturn).NotNull().GreaterThan(y => y.timeOfRental).WithMessage("{PropertyName} when added, cannot be null or be an earlier date than rental start time ('timeOfRental')").When(m => m.timeOfReturn.HasValue || m.finished == true);

            RuleFor(x => x.finished).Equal(true).When(m => m.carMilageAfter.HasValue && m.timeOfReturn.HasValue); //could be better
            #endregion
        }
    }
}