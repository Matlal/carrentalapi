﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RentalAPI.Models
{
    public class RentalAPIContext : DbContext, IRentalAPIContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public RentalAPIContext() : base("name=RentalAPIContext")
        {
        }

        public DbSet<Rental> Rentals { get; set; }
        public DbSet<CarRentalCategory> CarRentalCategories { get; set; }
        public DbSet<RentalRate> RentalRates { get; set; }


        

        public void MarkAsModified(Rental item)
        {
            Entry(item).State = EntityState.Modified;
        }
    }
}
