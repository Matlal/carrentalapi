namespace RentalAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CarRentalCategories",
                c => new
                    {
                        CategoryId = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 450, unicode: false),
                        createdAt = c.DateTime(nullable: false),
                        updatedAt = c.DateTime(),
                        RentalRateId = c.Int(nullable: false),
                        baseDayRentalVariable = c.Decimal(precision: 18, scale: 2),
                        distanceVariable = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.CategoryId)
                .ForeignKey("dbo.RentalRates", t => t.RentalRateId, cascadeDelete: true)
                .Index(t => t.name, unique: true)
                .Index(t => t.RentalRateId);
            
            CreateTable(
                "dbo.RentalRates",
                c => new
                    {
                        RentalRateId = c.Int(nullable: false, identity: true),
                        createdAt = c.DateTime(nullable: false),
                        updatedAt = c.DateTime(),
                        baseDayRental = c.Decimal(nullable: false, precision: 18, scale: 2),
                        kmPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.RentalRateId);
            
            CreateTable(
                "dbo.Rentals",
                c => new
                    {
                        RentalId = c.Int(nullable: false, identity: true),
                        createdAt = c.DateTime(nullable: false),
                        updatedAt = c.DateTime(),
                        CategoryId = c.Int(nullable: false),
                        bookingNumber = c.Int(nullable: false),
                        carMilageBefore = c.Decimal(nullable: false, precision: 18, scale: 2),
                        timeOfRental = c.DateTime(nullable: false),
                        customerDateOfBirth = c.DateTime(nullable: false),
                        carMilageAfter = c.Decimal(precision: 18, scale: 2),
                        timeOfReturn = c.DateTime(),
                        finished = c.Boolean(nullable: false),
                        price = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.RentalId)
                .ForeignKey("dbo.CarRentalCategories", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId)
                .Index(t => t.bookingNumber, unique: true);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Rentals", "CategoryId", "dbo.CarRentalCategories");
            DropForeignKey("dbo.CarRentalCategories", "RentalRateId", "dbo.RentalRates");
            DropIndex("dbo.Rentals", new[] { "bookingNumber" });
            DropIndex("dbo.Rentals", new[] { "CategoryId" });
            DropIndex("dbo.CarRentalCategories", new[] { "RentalRateId" });
            DropIndex("dbo.CarRentalCategories", new[] { "name" });
            DropTable("dbo.Rentals");
            DropTable("dbo.RentalRates");
            DropTable("dbo.CarRentalCategories");
        }
    }
}
