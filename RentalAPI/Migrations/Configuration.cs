namespace RentalAPI.Migrations
{
    using RentalAPI.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<RentalAPI.Models.RentalAPIContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RentalAPI.Models.RentalAPIContext context)
        {
            try
            {
                // Your code...
                // Could also be before try if you know the exception occurs in SaveChanges
                //  This method will be called after migrating to the latest version.

                //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
                //  to avoid creating duplicate seed data. E.g.
                //
                //    context.People.AddOrUpdate(
                //      p => p.FullName,
                //      new Person { FullName = "Andrew Peters" },
                //      new Person { FullName = "Brice Lambson" },
                //      new Person { FullName = "Rowan Miller" }
                //    );
                //baseDayRentalVariable=0.0m, distanceVariable=0.0m
                Console.WriteLine("hello0");
                context.RentalRates.AddOrUpdate(x => x.RentalRateId, new RentalRate[]{
                    new RentalRate() { RentalRateId=1, createdAt=DateTime.Now, updatedAt=DateTime.Now,  baseDayRental=0.0m, kmPrice=0.0m },
                    new RentalRate() { RentalRateId=2, createdAt=DateTime.Now, updatedAt=DateTime.Now, baseDayRental=1.0m, kmPrice=1.0m  }
                });
                int lastRateId = context.RentalRates.OrderByDescending(r => r.RentalRateId).FirstOrDefault().RentalRateId;
                context.CarRentalCategories.AddOrUpdate(x => x.CategoryId, new CarRentalCategory[] {
                    new CarRentalCategory() { CategoryId=1,createdAt=DateTime.Now, updatedAt=DateTime.Now, name="Small car",
                        RentalRateId=lastRateId, baseDayRentalVariable=0.0m, distanceVariable=0.0m  },
                    new CarRentalCategory() { CategoryId=2,createdAt=DateTime.Now, updatedAt=DateTime.Now, name="Van",
                        RentalRateId =lastRateId, baseDayRentalVariable=1.2m, distanceVariable=0.0m  },
                    new CarRentalCategory() { CategoryId=3, createdAt=DateTime.Now, updatedAt=DateTime.Now, name="Mini bus",
                        RentalRateId =lastRateId, baseDayRentalVariable=1.7m, distanceVariable=1.5m  }
                    });


                Console.WriteLine("hello");

                context.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                Console.WriteLine("hello2");
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }
    }
}
