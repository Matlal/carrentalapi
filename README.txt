# CarRentalAPI

Detta är resultatet av det Utvecklar-test som undertecknad blev uppmanad att utföra.
Resultatet blev utformat som ett web api (tänkt endast att köras på en server) och inget grafik är utvecklad. 

Koden kan inspekteras och laddas ner på två sätt: 
-  via BitBucket och där projektet är lagrat, följ [denna länk](https://bitbucket.org/Matlal/carrentalapi).  
-  via Google Docs som en komprimerad (.zip-)fil, följ [denna länk](https://drive.google.com/drive/folders/1ADY2gCKIkQiuyqj8wW-Fx1ix8OMd9G0H?usp=sharing).



Om framtida ändringar sker, kommer dessa endast att synas via Bitbucket.
Testinstruktionerna går att hitta i samma mapp som denna fil.  
# Koden
### Förutsättningar
 - Windows 10
 - Visual Studio 17 community edition  -    version 15.5.7
 - .Net Framework 4.5
 - ASP .NET and Web Tools 2017, 
 - ASP .NET Web Framworks and Tools 2017, v. 5.2

Koden har inte testats i andra miljöer och kan endast försvaras under matchande förutsättningar. Inga garantier om att kompilering går att utföras i andra miljöer, kan ges. 


### Köra koden
För dem som är bekanta med versionshanteringsprogrammet Git, kan man snabbt ladda ner hela och klona projektet genom att kopiera och klistra in följande kod i en terminal:

    git clone https://Matlal@bitbucket.org/Matlal/carrentalapi.git

- För att köra koden kan man trycka på grön pil i översta menyn eller trycka knappen `F5`. Därefter bör den förvalda webbläsaren starta en ny flik med projektets url `localhost:port/api/rentals`. Portnumret utses vid varje körning, för egen del vart det ofta 51832. 
- Körning utan 'debugg-läge' går att starta genom att trycka `ctrl+F5`.

### Tester

Testerna är lagda i ett eget projekt.

1. För att överblicka testerna kan man klicka fram sidovyn 'Test Explorer'. Gör detta genom att trycka på menyfliken 'Test', 'Windows' och slutligen "Test Explorer".
2. Välj att köra eller 'debugga' alla eller enskilda test-metoder. 



--
Skrivet av Mattias