﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RentalAPI.Controllers;
using RentalAPI.Models;
using System;
using System.Linq;
using System.Net;
using System.Web.Http.Results;

namespace RentalAPI.Tests
{
    [TestClass]
    public class TestRentalController
    {
        private RentalValidator validator ;
        private TestRentalAPIContext context;
        private RentalsController ctrl;
        public void SetupControllerTest()
        {
            validator = new RentalValidator();
            context = new TestRentalAPIContext();
            ctrl= new RentalsController(context);
        }

        #region GET
        [TestMethod]
        public void GetRental_ShouldReturnRentalWithSameID()
        {
            SetupControllerTest();
            context.Rentals.Add(GetDemoRental());
            context.Rentals.Add(GetDemoRental());
            context.Rentals.Add(GetDemoRental());
            var result = ctrl.GetRental(3) as OkNegotiatedContentResult<Rental>;

            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Content.RentalId);
        }

        [TestMethod]
        public void GetRentals_ShouldReturnAllRentals()
        {
            SetupControllerTest();
            context.Rentals.Add(GetDemoRental(1));
            context.Rentals.Add(GetDemoRental(2));
            context.Rentals.Add(GetDemoRental(3)); //RentalId 3 should have CategoryId:3
            var result = ctrl.GetAllRental() as TestRentalDbSet;

            Assert.IsNotNull(result);
            Assert.AreEqual(result.FirstOrDefault(x => x.RentalId == 3).carRentalCategory.CategoryId, 3);
            Assert.AreEqual(3, result.Local.Count);
        }

        #endregion
        #region POST
        [TestMethod]
        public void PostRental_ShouldFailOnInvalidModelState()
        {
            SetupControllerTest();
            var item = new Rental() { };
            var count = context.Rentals.Count();
            var validation = validator.Validate(item);
            ctrl.ModelState.AddModelError("error", validation.Errors.First().ErrorMessage);
            var result = ctrl.Post(item);
            Assert.AreEqual(context.Rentals.Count(), count);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        [TestMethod]
        public void PostRental_ShouldAddRentalAndReturnSameRental()
        {
            SetupControllerTest();
            var item = GetDemoRental();
            int count= context.Rentals.Count();
            var result =ctrl.Post(item) as CreatedAtRouteNegotiatedContentResult<Rental>;

            Assert.IsNotNull(result);
            Assert.AreEqual(count + 1, context.Rentals.Count());
            Assert.AreEqual(result.RouteName, "RentalAPI");
            Assert.AreEqual(result.RouteValues["id"], result.Content.RentalId);
            Assert.AreEqual(result.Content.bookingNumber, item.bookingNumber);
        }
        #endregion
        #region PUT
        [TestMethod]
        public void PutRental_ShouldReturnNullWhenTrying2UpdateNonExistingRental()
        {
            SetupControllerTest();
            var item = GetDemoRental();
            
            var result = ctrl.PutRental(item.bookingNumber, item) as StatusCodeResult;
            Assert.IsTrue(context.Rentals.Count() == 0);
            Assert.IsNull(result);
        }
        [TestMethod]
        public void PutRental_ShouldFailWhenDifferentID()
        {
            SetupControllerTest();
            var badresult = ctrl.PutRental(-1, GetDemoRental()) as BadRequestErrorMessageResult;
            Assert.IsInstanceOfType(badresult, typeof(BadRequestErrorMessageResult));
            Assert.AreEqual("non mathing ids", badresult.Message);
        }
        [TestMethod]
        public void PutRental_ShouldReturnOk()
        {
            SetupControllerTest();
            var item = GetDemoRental();
           
            context.Rentals.Add(item);
            
            item.customerDateOfBirth = new DateTime(2016,9,12);
            var result = ctrl.PutRental(item.RentalId, item) as StatusCodeResult;
            var firstObj=context.Rentals.First();
            Assert.AreEqual(firstObj, item);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(StatusCodeResult));
            Assert.AreEqual(HttpStatusCode.NoContent, result.StatusCode);
        }
        
        [TestMethod]
        public void PutRentalReturn_ShouldFailIfRentalIsFinished()
        {
            SetupControllerTest();
            var item = GetDemoRental();
            ctrl.Post(item);
            item.carMilageAfter = 551.0m;
            item.timeOfReturn = new DateTime(2018, 9, 9);
            item.finished = true;
            ctrl.PutRentalReturn(item.bookingNumber, item);
            item.price = 300;
            var result = ctrl.PutRental(item.bookingNumber, item) as BadRequestErrorMessageResult;
            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));
            Assert.AreEqual("The rental has already been returned!", result.Message);
        }
        [TestMethod]
        public void PutRentalReturn_ShouldFailIfRentalIsInvalid()
        {
            SetupControllerTest();
            Rental item = GetDemoRental();
            ctrl.Post(item);

            //bad carMilageAfter
            item.carMilageAfter = 451.0m;
            item.timeOfReturn = new DateTime(2018, 9, 9);
            item.finished = true;
            var validation = validator.Validate(item);
            ctrl.ModelState.AddModelError("error", validation.Errors.First().ErrorMessage);
            var result = ctrl.PutRentalReturn(item.bookingNumber, item);
            Rental storedObj=context.Rentals.First(x => x.bookingNumber == item.bookingNumber);
            
            Assert.AreNotEqual(item.carMilageAfter, storedObj.carMilageAfter);

            //bad timeOfReturn
            item.carMilageAfter = 600.0m;
            item.timeOfReturn = new DateTime(2016, 9, 9);
            
            validation = validator.Validate(item);
            ctrl.ModelState.AddModelError("error", validation.Errors.First().ErrorMessage);
            result = ctrl.PutRentalReturn(item.bookingNumber, item);
            storedObj = context.Rentals.FirstOrDefault(x => x.bookingNumber == item.bookingNumber);
            Assert.AreNotEqual(item.timeOfReturn, storedObj.timeOfReturn);

            //finished not true
            item.timeOfReturn = new DateTime(2018, 9, 9);
            item.finished = false;
            validation = validator.Validate(item);
            ctrl.ModelState.AddModelError("error", validation.Errors.First().ErrorMessage);
            result = ctrl.PutRentalReturn(item.bookingNumber, item);
            storedObj = context.Rentals.FirstOrDefault(x => x.bookingNumber == item.bookingNumber);
            Assert.AreNotEqual(item.timeOfReturn, storedObj.timeOfReturn);

        }
        [TestMethod]
        public void PutRentalReturn_ShouldCalculatePriceOnValidReturn()
        {
            SetupControllerTest();
            Rental item = GetDemoRental();
            ctrl.Post(item);

            
            item.carMilageAfter = 551.0m;
            item.timeOfReturn = new DateTime(2018, 9, 9);
            item.finished = true;
            var result = ctrl.PutRentalReturn(item.bookingNumber, item);
            Rental storedObj = context.Rentals.First(x => x.bookingNumber == item.bookingNumber);

            Assert.AreEqual(item.carMilageAfter, storedObj.carMilageAfter);
            Assert.AreEqual(item.timeOfReturn, storedObj.timeOfReturn);
            Assert.AreEqual(item.finished, storedObj.finished);
            Assert.AreEqual(calculatePrice(item),storedObj.price);

        }

        private decimal calculatePrice(Rental rental)
        {
            var time = rental.timeOfReturn ?? DateTime.Now;
            var numberofdays = new Decimal((time.Date - rental.timeOfRental.Date).TotalDays);
            var milage = rental.carMilageAfter - rental.carMilageBefore;
            decimal timeVariable = rental.carRentalCategory.baseDayRentalVariable.HasValue && rental.carRentalCategory.baseDayRentalVariable > 0 ? (decimal)rental.carRentalCategory.baseDayRentalVariable : 1;
            decimal distVariable = rental.carRentalCategory.distanceVariable.HasValue && rental.carRentalCategory.baseDayRentalVariable > 0 ? (decimal)rental.carRentalCategory.baseDayRentalVariable : 1;
            rental.price =
                rental.carRentalCategory.rentalRate.baseDayRental * (numberofdays) * timeVariable
                +
                rental.carRentalCategory.rentalRate.kmPrice * milage * distVariable;
            return (decimal)rental.price;
        }

        #endregion
        #region DELETE

        [TestMethod]
        public void DeleteRental_ShouldReturnOK()
        {
            SetupControllerTest();
            var item = GetDemoRental();
            context.Rentals.Add(item);
            int size = context.Rentals.Count();
            var result = ctrl.DeleteRental(1) as OkNegotiatedContentResult<Rental>;
            var sizeAfter = context.Rentals.Count();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(size, sizeAfter);
            Assert.AreEqual(size-1, sizeAfter);
        }
        [TestMethod]
        public void DeleteRental_ShouldFailOnBadInput()
        {
            SetupControllerTest();
            var item = GetDemoRental();
            context.Rentals.Add(item);
            int size = context.Rentals.Count();
            var result = ctrl.DeleteRental(55);
            var sizeAfter = context.Rentals.Count();
            
            Assert.IsNotNull(result); 
            Assert.AreEqual(size, sizeAfter);
        }

        #endregion

        [TestMethod]
        public void getBookingNumber_ShouldReturnAnUniqueInt()
        {
            SetupControllerTest();
            var result=ctrl.getUniqueBookingNumber(context.Rentals);
            var item = context.Rentals.FirstOrDefault(x => x.bookingNumber == result);

            Assert.AreNotEqual(result, item);
        }
        

        Rental GetDemoRental(int carRentalCategoryId=1)
        {
            var _category = context.CarRentalCategories.Local.FirstOrDefault(x => x.CategoryId == carRentalCategoryId);
            var nr = ctrl.getUniqueBookingNumber(context.Rentals);
            return new Rental()
            {
                RentalId = ctrl.getUniqueId(context.Rentals),
                CategoryId = _category.CategoryId,
                
                createdAt = DateTime.Now,
                updatedAt=DateTime.Now,
                carRentalCategory = _category,
                bookingNumber =nr ,
                carMilageBefore = 500.0m,
                timeOfRental = DateTime.Now,
                customerDateOfBirth = new DateTime(2011, 08, 07)
            };
        }
        
        //protected RentalAPI.Models.RentalAPIContext dataContext;

        //[TestInitialize]
        //public void InitTest()
        //{
        //    dataContext = new Models.RentalAPIContext();
        //    dataContext.RentalRates.AddRange(new RentalRate[] { new RentalRate() { RentalRateId=1, createdAt=DateTime.Now, updatedAt=DateTime.Now,  baseDayRental=0.0m, kmPrice=0.0m },
        //            new RentalRate() { RentalRateId=2, createdAt=DateTime.Now, updatedAt=DateTime.Now, baseDayRental=1.0m, kmPrice=1.0m  }});
        //    int lastRateId = dataContext.RentalRates.OrderByDescending(r => r.RentalRateId).FirstOrDefault().RentalRateId;
        //    dataContext.CarRentalCategories.AddRange(new CarRentalCategory[] {
        //            new CarRentalCategory() { CategoryId=1,createdAt=DateTime.Now, updatedAt=DateTime.Now, name="Small car",
        //                RentalRateId=lastRateId, baseDayRentalVariable=0.0m, distanceVariable=0.0m  },
        //            new CarRentalCategory() { CategoryId=2,createdAt=DateTime.Now, updatedAt=DateTime.Now, name="Van",
        //                RentalRateId =lastRateId, baseDayRentalVariable=1.2m, distanceVariable=0.0m  },
        //            new CarRentalCategory() { CategoryId=3, createdAt=DateTime.Now, updatedAt=DateTime.Now, name="Mini bus",
        //                RentalRateId =lastRateId, baseDayRentalVariable=1.7m, distanceVariable=1.5m  }
        //            });
        //}

        //[Route("api/rental/init-test")]
        //public async Task<IHttpActionResult> testPost()
        //{
        //    Rental obj = new Rental()
        //    {
        //        CategoryId = 1,
        //        carRentalCategory = db.CarRentalCategories.FirstOrDefault(x => x.CategoryId == 1),
        //        bookingNumber = db.getBookingNumber(),
        //        carMilageBefore = 500.0m,
        //        timeOfRental = DateTime.Now,
        //        customerDateOfBirth = new DateTime(2011, 08, 07),

        //    };
        //    PostRental(obj);
        //    await db.SaveChangesAsync();
        //    return CreatedAtRoute("RentalAPI", new { id = obj.RentalId }, obj);
        //}
    }
}
