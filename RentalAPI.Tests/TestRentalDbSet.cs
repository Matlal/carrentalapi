﻿using RentalAPI.Models;
using System.Linq;

namespace RentalAPI.Tests
{
    class TestRentalDbSet:TestBase<Rental>
    {
        public override Rental Find(params object[] keyValues)
        {
            return this.SingleOrDefault(rental => rental.RentalId == (int)keyValues.Single());
        }
    }
}
