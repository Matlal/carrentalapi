﻿using RentalAPI.Models;
using System.Linq;

namespace RentalAPI.Tests
{
    class TestCarRentalCategoryDbSet : TestBase<CarRentalCategory>
    {
        public override CarRentalCategory Find(params object[] keyValues)
        {
            return this.SingleOrDefault(category => category.CategoryId == (int)keyValues.Single());
        }
    }
}
