﻿using RentalAPI.Models;
using System.Linq;

namespace RentalAPI.Tests
{
    class TestRentalRateDbSet : TestBase<RentalRate>
    {
        public override RentalRate Find(params object[] keyValues)
        {
            return this.SingleOrDefault(rr => rr.RentalRateId == (int)keyValues.Single());
        }
    }
}
