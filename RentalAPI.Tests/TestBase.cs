﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Data.Entity;

namespace RentalAPI.Tests
{
    /// <summary>
    /// Summary description for TestBase
    /// </summary>
    [TestClass]
    public class TestBase<T> : DbSet<T>, IQueryable, IEnumerable<T>
        where T : class
    {
        ObservableCollection<T> _data;
        IQueryable _query;

        public TestBase()
        {
            _data = new ObservableCollection<T>();
            _query = _data.AsQueryable();
        }

        public override T Add(T item)
        {
            _data.Add(item);
            return item;
        }

        public override T Remove(T item)
        {
            _data.Remove(item);
            return item;
        }

        public override T Attach(T item)
        {
            _data.Add(item);
            return item;
        }

        public override T Create()
        {
            return Activator.CreateInstance<T>();
        }

        public override TDerivedEntity Create<TDerivedEntity>()
        {
            return Activator.CreateInstance<TDerivedEntity>();
        }

        public override ObservableCollection<T> Local
        {
            get { return new ObservableCollection<T>(_data); }
        }

        Type IQueryable.ElementType
        {
            get { return _query.ElementType; }
        }

        System.Linq.Expressions.Expression IQueryable.Expression
        {
            get { return _query.Expression; }
        }

        IQueryProvider IQueryable.Provider
        {
            get { return _query.Provider; }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _data.GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return _data.GetEnumerator();
        }




        //protected RentalAPI.Models.RentalAPIContext dataContext;

        //[TestInitialize]
        //public void InitTest()
        //{
        //    dataContext = new Models.RentalAPIContext();
        //    dataContext.RentalRates.AddRange(new RentalRate[] { new RentalRate() { RentalRateId=1, createdAt=DateTime.Now, updatedAt=DateTime.Now,  baseDayRental=0.0m, kmPrice=0.0m },
        //            new RentalRate() { RentalRateId=2, createdAt=DateTime.Now, updatedAt=DateTime.Now, baseDayRental=1.0m, kmPrice=1.0m  }});
        //    int lastRateId = dataContext.RentalRates.OrderByDescending(r => r.RentalRateId).FirstOrDefault().RentalRateId;
        //    dataContext.CarRentalCategories.AddRange(new CarRentalCategory[] {
        //            new CarRentalCategory() { CategoryId=1,createdAt=DateTime.Now, updatedAt=DateTime.Now, name="Small car",
        //                RentalRateId=lastRateId, baseDayRentalVariable=0.0m, distanceVariable=0.0m  },
        //            new CarRentalCategory() { CategoryId=2,createdAt=DateTime.Now, updatedAt=DateTime.Now, name="Van",
        //                RentalRateId =lastRateId, baseDayRentalVariable=1.2m, distanceVariable=0.0m  },
        //            new CarRentalCategory() { CategoryId=3, createdAt=DateTime.Now, updatedAt=DateTime.Now, name="Mini bus",
        //                RentalRateId =lastRateId, baseDayRentalVariable=1.7m, distanceVariable=1.5m  }
        //            });
        //}

      
    }
}
