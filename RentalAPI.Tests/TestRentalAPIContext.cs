﻿using RentalAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentalAPI.Tests
{
    public class TestRentalAPIContext : IRentalAPIContext
    {
        public TestRentalAPIContext()
        {
            Rentals = new TestRentalDbSet();
            CarRentalCategories = new TestCarRentalCategoryDbSet();
            RentalRates = new TestRentalRateDbSet();
            initRentalRates();
            initDemoCategories();
        }


        public DbSet<Rental> Rentals { get; set; }
        public DbSet<CarRentalCategory> CarRentalCategories { get; set; }
        public DbSet<RentalRate> RentalRates { get; set; }

        public int SaveChanges()
        {
            return 0;
        }

        public void MarkAsModified(Rental item) { }
        public void Dispose() { }

        void initRentalRates()
        {
            RentalRates.Add(new RentalRate() { RentalRateId = 1, createdAt = DateTime.Now, updatedAt = DateTime.Now, baseDayRental = 0.0m, kmPrice = 0.0m });
           RentalRates.Add(new RentalRate() { RentalRateId=2, createdAt=DateTime.Now, updatedAt=DateTime.Now, baseDayRental=1.0m, kmPrice=1.0m  });
        }
        void initDemoCategories()
        {
            RentalRate lastRate= RentalRates.OrderByDescending(r => r.RentalRateId).FirstOrDefault();
            CarRentalCategories.Add(new CarRentalCategory() { CategoryId = 1, createdAt = DateTime.Now, updatedAt = DateTime.Now, name = "Small car",
                RentalRateId = lastRate.RentalRateId, rentalRate=lastRate, baseDayRentalVariable = 0.0m, distanceVariable = 0.0m });
            CarRentalCategories.Add(new CarRentalCategory() { CategoryId = 2, createdAt = DateTime.Now, updatedAt = DateTime.Now, name = "Van",
                RentalRateId = lastRate.RentalRateId, rentalRate=lastRate, baseDayRentalVariable = 1.2m, distanceVariable = 0.0m });
            CarRentalCategories.Add(new CarRentalCategory() { CategoryId=3, createdAt=DateTime.Now, updatedAt=DateTime.Now, name="Mini bus",
                RentalRateId = lastRate.RentalRateId, rentalRate = lastRate, baseDayRentalVariable=1.7m, distanceVariable=1.5m  });
        }

      
    }
}
